# What Is the Dark Web? How Many Parts Are on Dark Web? #

A dark world of the Internet is the dark part of online which ordinary people do not know anything about; today we are going to tell about the dark web. We are talking about the dark web which is a part of the internet where we never go. Today you will know what the dark web is and how does it work and what happens here.

What is the dark web?

It is also called Dark net, it is a part of the World Wide Web of the Internet, but it is not a small part, 96% of the Internet is Dark Web but still not everyone knows about it and one reason behind it is also that we cannot reach there directly through the browser. All the websites and contents of the dark web are encrypted, i.e. hidden and cannot be accessed by search engines like Google.

There are three parts of World Wide Web (WWW):
1. Surface web
2. Deep web
3. Dark web

Now let's understand these three in some detail.

What is Surface web?

Generally, the part of the Internet that we and you use in everyday life is called the Surface Web. All the websites that we access through search engines like Google, Bing, Yahoo, all fall in this part. You will be surprised to know that only 4% of the entire internet is surface web. That is, we use only 4% of the Internet.

Such web pages that anyone can access publicly are in this region of the same Internet. To open these sites no special software or configuration is required nor is any permission required. We can easily access it from browsers like Google Chrome, Firefox, Opera.

What is deep web?

Web pages on the internet which are not indexed by search engines, i.e. they are out of reach of the search engine and we have to log in to access it. These pages cannot be viewed without permission.

Just like you cannot read emails without logging into your Gmail account, without creating an account you cannot see your friend's profile page on Facebook. All these web pages are within the deep web itself. You can see their content, but for that you must have an ID and password.

What is dark web?

We cannot access it from a simple browser. Nor can we reach there through Google; we cannot reach this area through any common search engine. The IPs of websites in the dark web is encrypted through the encryption tool, that is why they are not seen by the search engines.

A special type of browser called Tor browser is used to access these hidden sites. These sites can be accessed only by people who know about it. It is the favorite place of criminals and hackers where all kinds of illegal transactions, drugs smuggling, pornography, arms sale, human trafficking, and selling stolen credit card details are committed.

* [https://www.onsist.com/threat-intelligence](https://www.onsist.com/threat-intelligence)

